from fastapi import FastAPI, HTTPException, status
from pydantic import BaseModel
from typing import Optional
import uuid

app = FastAPI()

users = {}
registrations = {}

class RegistrationRequest(BaseModel):
    phone_number: str

class AuthRequest(BaseModel):
    session_id: str
    otp: str

class RegistrationCompleteRequest(BaseModel):
    session_id: str
    nickname: str

@app.post("/auth/register")
def register(request: RegistrationRequest):
    session_id = str(uuid.uuid4())

    registrations[session_id] = {"phone_number": request.phone_number, "verified": False}
    return {"session_id": session_id, "is_registered": request.phone_number in users}

@app.post("/auth/verify")
def verify(auth_request: AuthRequest):
    registration = registrations.get(auth_request.session_id)
    if not registration:
        raise HTTPException(status_code=404, detail="Registration not found")

    if auth_request.otp == "123456":
        registration["verified"] = True
        return {"message": "OTP verified successfully"}
    else:
        raise HTTPException(status_code=400, detail="Invalid OTP")

@app.post("/auth/complete-registration")
def complete_registration(request: RegistrationCompleteRequest):
    registration = registrations.get(request.session_id)
    if not registration or not registration.get("verified"):
        raise HTTPException(status_code=400, detail="Registration not valid or not verified")

    users[registration["phone_number"]] = {"nickname": request.nickname, "phone_number": registration["phone_number"], "user_id": request.session_id}
    return {"message": "Registration completed successfully", "user_id": request.session_id}

from typing import List, Optional
from pydantic import BaseModel

class CarFilter(BaseModel):
    brand: str
    model: str
    year: List[int]
    price: List[float]
    run: List[float]
    equipment: str

class FilterResponse(BaseModel):
    filter_id: str
    count: int

class CarAd(BaseModel):
    brand: str
    model: str
    year: int
    run: int
    price: float
    photo_urls: List[str]
    contact_user_id: str

class CarAdList(BaseModel):
    ads: List[CarAd]

filters = {}
ads = [
    {
        "brand": "Toyota",
        "model": "Camry",
        "year": 2018,
        "run": 45000,
        "price": 15000.0,
        "photo_urls": ["https://example.com/toyota_camry_1.jpg", "https://example.com/toyota_camry_2.jpg"],
        "contact_user_id": "user123"
    },
    {
        "brand": "Honda",
        "model": "Accord",
        "year": 2017,
        "run": 60000,
        "price": 14000.0,
        "photo_urls": ["https://example.com/honda_accord_1.jpg", "https://example.com/honda_accord_2.jpg"],
        "contact_user_id": "user456"
    },
    {
        "brand": "Ford",
        "model": "Fusion",
        "year": 2019,
        "run": 30000,
        "price": 17000.0,
        "photo_urls": ["https://example.com/ford_fusion_1.jpg", "https://example.com/ford_fusion_2.jpg"],
        "contact_user_id": "user789"
    },
    {
        "brand": "Chevrolet",
        "model": "Malibu",
        "year": 2016,
        "run": 70000,
        "price": 12000.0,
        "photo_urls": ["https://example.com/chevrolet_malibu_1.jpg", "https://example.com/chevrolet_malibu_2.jpg"],
        "contact_user_id": "user1011"
    },
    {
        "brand": "Nissan",
        "model": "Altima",
        "year": 2020,
        "run": 20000,
        "price": 20000.0,
        "photo_urls": ["https://example.com/nissan_altima_1.jpg", "https://example.com/nissan_altima_2.jpg"],
        "contact_user_id": "user1213"
    }
]


@app.post("/filters/", response_model=FilterResponse)
def create_filter(filter_details: CarFilter):
    filter_id = str(uuid.uuid4())

    found_ads_count = 0  
    filters[filter_id] = filter_details.model_dump()
    return {"filter_id": filter_id, "count": found_ads_count}

@app.get("/ads/", response_model=CarAdList)
def get_ads(filter_id: str, offset: int = 0, limit: int = 10):
    filtered_ads = ads[offset:offset+limit]
    return CarAdList(ads=filtered_ads)


if __name__ == "__main__":
    import uvicorn

    uvicorn.run("app:app", host="0.0.0.0", port=80, reload=True)
